<?php

namespace App\Http\Controllers;

use App\SW0102645;
use Illuminate\Http\Request;

class SW0102645Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semester = SW0102645::all();
        $i = 1;
        return view('SW0102645.index', compact('semester', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('SW0102645.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'year' => 'required|integer|integer|between:2018,2025',
            'semester' => 'required|integer',
            'cgpa' => 'required|numeric|between:0,4.00'
        ]);

        SW0102645::create([
            'year' => $validatedData['year'],
            'semester' => $validatedData['semester'],
            'cgpa' => $validatedData['cgpa'],
        ]);

        return redirect('sw0102645')->with('success', 'New semester has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SW0102645  $sW0102645
     * @return \Illuminate\Http\Response
     */
    public function show(SW0102645 $sW0102645)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SW0102645  $sW0102645
     * @return \Illuminate\Http\Response
     */
    public function edit(SW0102645 $sW0102645)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SW0102645  $sW0102645
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SW0102645 $sW0102645)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SW0102645  $sW0102645
     * @return \Illuminate\Http\Response
     */
    public function destroy(SW0102645 $sW0102645)
    {
        //
    }
}
