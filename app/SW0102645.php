<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SW0102645 extends Model
{
    protected $fillable = [
        'year', 'semester', 'cgpa',
    ];
}
