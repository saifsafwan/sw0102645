@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Semester</div>

                @if(session()->get('success'))
                <div class="alert alert-success">
                {{ session()->get('success') }}
                </div>
                @endif

                <div class="card-body">
                    <div class="text-center">
                        <a href="{{ route('sw0102645.create') }}" class="btn btn-primary mb-3">Insert New Semester</a>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Year</th>
                            <th scope="col">Semester</th>
                            <th scope="col">CGPA</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($semester as $sem)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$sem->year}}</td>
                                <td>{{$sem->semester}}</td>
                                <td>{{$sem->cgpa}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


  @endsection