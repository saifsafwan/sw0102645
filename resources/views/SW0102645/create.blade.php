@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Semester</div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                    @endif
                    <form action="{{route('sw0102645.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="year">Year</label>
                            <input type="number" name="year" class="form-control" id="year" placeholder="Enter year">
                            <small id="yearHelp" class="form-text text-muted">The year must be between 2018 and 2025.</small>
                        </div>
                        <div class="form-group">
                            <label for="semester">Semester</label>
                            <input type="number" name="semester" class="form-control" id="semester" placeholder="Enter semester">
                            <small id="semesterHelp" class="form-text text-muted">Enter number only. eg: 1/2/3</small>
                        </div>
                        <div class="form-group">
                            <label for="cgpa">CGPA</label>
                            <input type="number" name="cgpa" class="form-control" id="cgpa" placeholder="Enter CGPA" step="0.01">
                            <small id="semesterHelp" class="form-text text-muted">Enter number only. eg: 3.89</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection